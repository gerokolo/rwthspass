/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zinseszinsrechner;

/**
 *
 * @author gerok
 */
public class Zinseszinszrechner {

   
    public static void Ziel(double startWert, double zinsSatz){
          
        double zielWert = SimpleIO.getDouble("Was ist ihr Zielwert?");
        double zähler = startWert;
        int counter = 0;
          
        while(zähler<zielWert){
            
            counter++;
            zähler += zähler * zinsSatz;
           
          }
        SimpleIO.output("Es dauert " + counter + " Jahre bei einem Zinssatz von " + zinsSatz * 100 + "% um von von " + startWert + " auf einen Betrag von " + zielWert + " zu sparen", "Ausgabe");
      
    }
      
    public static void Zeit(double startWert, double zinsSatz){
          
        
        int zeit = SimpleIO.getInt("Bitte geben sie eine Anzahl von Jahren ein");
        for(int i = 0; i<zeit; i++){
              
            startWert += startWert * zinsSatz;
              
        }
        SimpleIO.output( startWert + "" , "Ihr Wert beträgt nun");
      
    }
    

    
    /**
     * @param args the command line arguments
     */
  
    public static void main(String[] args) {
          
      double startWert = SimpleIO.getDouble("Bitte geben sie den Startwert ein");
      double zinsSatz = SimpleIO.getDouble("Bitte geben sie den Zinssatz in Prozent ein")/100;
      String s = SimpleIO.getString("Was wollen sie berechnen: Ziel oder Zeit");
    
      switch (s) {
        case "Zeit":
           Zeit(startWert, zinsSatz);
           break;
        case "Ziel":
           Ziel(startWert,zinsSatz);
           break;
        default:
           System.exit(0);
      }      
   }
}
